package com.qaconsultants;

import com.qaconsultants.processing.Node;
import com.qaconsultants.processing.Processor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

import static org.junit.Assert.assertTrue;

@RunWith(Parameterized.class)
public class ProcessorTest {
    @Parameterized.Parameters
    public static Iterable<? extends Object> data() {
        return Arrays.asList(new Node(null, "invalid", 0),
                             new Node(null, "https://www.httpbin.org/", 0),
                             new Node(null, "https://www.thisisnotrealweb.com/", 0),
                             new Node(null, null, 0));
    }

    @Parameterized.Parameter
    public Node _parameter;

    @Test
    public void testConstructor(){
       assertTrue(true);
    }

    @Test
    public void testProcessing() throws Exception {
        Processor.Configuration _config = new Processor.Configuration();
        Processor _processor = new Processor(_config);
       // _processor.process(_parameter);
    }
}
