package com.qaconsultants;

import com.moandjiezana.toml.Toml;
import com.qaconsultants.processing.ScreenshotPostProcessor;
import com.qaconsultants.processing.strategy.ConfigurationProcessorStrategy;
import com.qaconsultants.processing.Node;
import com.qaconsultants.processing.Processor;
import com.qaconsultants.processing.watcher.ConsoleFileWatcher;
import com.qaconsultants.utility.DriverFactory;
import com.qaconsultants.utility.ZipUtils;
import org.json.JSONArray;
import org.json.JSONML;
import org.json.JSONObject;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class App {

	//HACKY WAY OF VERSIONING BUT IT WORKS DONT FORGET TO UPDATE THIS
	public static String   version = "1.1.1";
    public static Calendar calendar = Calendar.getInstance();

	/**
	 * Main Program Entry Point
	 * @param args the command line arguments
	 * @throws Exception an exception that may be thrown
	 */
    public static void main( String[] args ) throws Exception {

        // The first and only argument should be a Configuration file
		String _configPath  = "./resources/configuration/Config.toml";
		if(args.length > 0 && Files.exists(Paths.get(args[0]))) {
			_configPath = args[0];
		} else {
			System.out.println(String.format("Configuration path is missing or invalid - Using default path: %s", _configPath));
		}

		// Read a configuration file. If not found, throws FileNotFoundException
		Config _config    = new Toml().read(new File(_configPath)).to(Config.class);
		String _outputDir = _config.pathing.outputDir + getTimestampString("yyyy-MM-dd/HH-mm-ss/");
		String _zipPath   = _config.pathing.outputDir + "link-checker-" + version + "-" + getTimestampString("yyyy-MM-dd-HH-mm-ss") + ".zip";

		// Set the system property for the DriverFactory to locate WebDriver binaries in
		System.setProperty("qac.webdriverDirectory", _config.pathing.webdrivers);

		/*
		 * Create a new Processor Configuration object and load it with the values provided by the
		 * TOML configuration file prior to passing it to the Processor.
		 */
		Processor.Configuration _processorConfig = new Processor.Configuration();
		_processorConfig.setDepth(_config.linkchecker.maxDepth);
		_processorConfig.setSleep(_config.linkchecker.sleepTimeMS);

		// Create a Processor instance and IWatcher instance for the Processor to write events
		ConsoleFileWatcher _watcher   = new ConsoleFileWatcher(_outputDir + "logs/");
		Processor          _processor = new Processor(new ConfigurationProcessorStrategy(_config), _processorConfig);

		// Create the root Node object
		Node _root = new Node(null, _config.linkchecker.url, 0);

		// Begin the processor with our root, browser driver, and specified watcher
		Processor.ResultSet _results = _processor.process(_root, createDriver(_config), _watcher);
		_watcher.endProcessor(_results);

		// Take screenshots if the processor produced a list of broken nodes
		if (!_results.foundBrokenNodes.isEmpty()) {
			new ScreenshotPostProcessor(_outputDir, createDriver(_config)).postProcess(_results.foundBrokenNodes, _watcher);
		} else {
			_watcher.writeLog("No broken nodes found during processing. Skipping screenshots.");
		}

		// Generate a JSON file from the Processor contents
		//_watcher.writeLog("Creating JSON output file..(DEV USE)");
		//createJSON(_results, _config, _outputDir);

		// Pack output directory into a resulting .zip file and exit
        ZipUtils.pack(Paths.get(_outputDir), Paths.get(_zipPath));
		System.out.println("\nTool has completed execution");
    }

    private static void createJSON(Processor.ResultSet _results, Config _config, String _outputDir) throws IOException {
		JSONObject _process = new JSONObject();
        JSONArray  _objects = new JSONArray();

		for(Node n : _results.foundNodes){
			JSONObject _object = new JSONObject();
			_object.put("url", n.getURL());
			_object.put("status", n.getStatusCode());
			_object.put("selector", n.getElementSelector());

			ArrayList<Node> trace = n.traceToRoot();
			JSONObject _trace = new JSONObject();
			for(int i = 0; i < trace.size(); i++)
				_trace.put(Integer.toString(i), trace.get(i).getURL());
			_object.put("trace", _trace);
			_objects.put(_object);
		}
		_process.put("depth", _config.linkchecker.maxDepth);
		_process.put("initialURL", _config.linkchecker.url);
		_process.put("blacklist", _config.linkchecker.urlPatternBlacklist);
		_process.put("error codes", _config.linkchecker.errorCodes);
		_process.put("nodes", _objects);
		FileWriter _writer = new FileWriter(_outputDir + "output.json");
		_process.write(_writer);
		_writer.close();
	}

    private static WebDriver createDriver(Config _config) throws Exception {
    	if(!_config.remote.useRemote)
            return DriverFactory.getLocalDriver(_config.linkchecker.browser);
    	else {
			MutableCapabilities _caps = new MutableCapabilities(_config.remote.remoteCapabilities);
			return DriverFactory.getRemoteDriver(_caps, _config.remote.remoteAddress, _config.remote.remoteBrowser);
		}
	}

	/**
	 * Returns a string to be used as a directory naming convention
	 *
	 * @param _pattern Pattern to use for the SimpleDateFormat
	 * @return Dated directory structure (YYYY-MM-DD/HH-mm-ss)
	 */
	private static String getTimestampString(String _pattern){
		return new SimpleDateFormat(_pattern).format(calendar.getTime());
	}
}
