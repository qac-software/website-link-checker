package com.qaconsultants;

import java.util.HashMap;

/**
 * Configuration class that is mapped to by using the TOML4J reader.
 *
 * Each internal public class represents a TOML header with the variables within it representing
 * the options defined in the TOML file.
 */
public class Config
{
    /**
     * LinkChecker header
     */
    public class LinkChecker {
        // URL to check
        public String   url;
        // Browser to use
        public String   browser;
        // CSS Selectors to select on the given URL
        public String[] cssSelectors;
        // Valid URL schemes to look for (HTTP/HTTPS/ETC)
        public String[] validURLSchemes;
        // Ignored content types (Application/octet-stream, etc)
        public String[] ignoredContentTypes;
        // Ignored URLs when collecting children.
        public String[] urlPatternBlacklist;
        // Time between pings in MS. Used to prevent overloading servers.
        public int      sleepTimeMS;
        // Error codes to look for when validating links on a page
        public int[]    errorCodes;
        // Maximum search depth.
        // 1 = This page
        // 0 = This domain
        public int      maxDepth = 1;
    }

    /**
     * Pathing header
     */
    public class Pathing {
        // Path to the WebDriver executables
        public String webdrivers    = "./resources/webdrivers/";
        // Output directory of the resulting screenshots + logs
        public String outputDir     = "./output/";
    }

    public class Remote {
        public boolean useRemote = false;
        public String  remoteAddress;
        public String  remoteBrowser;
        public HashMap<String, String> remoteCapabilities;
    }

    // Local vars of the 'header' classes
    public LinkChecker  linkchecker;
    public Pathing      pathing;
    public Remote       remote;
}
