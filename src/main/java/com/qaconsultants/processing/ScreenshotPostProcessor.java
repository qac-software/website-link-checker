package com.qaconsultants.processing;

import com.qaconsultants.processing.watcher.ConsoleFileWatcher;
import com.qaconsultants.utility.FileNameCleaner;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class ScreenshotPostProcessor {
    private String     mOutputDirectory;
    private WebDriver  mWebDriver;
    private Integer[]  mIgnoreCodes =  new Integer[]{408, 401};

    /**
     * Default Constructor
     *
     * @param _saveDirectory Directory to save Screenshots in
     * @param _driver WebDriver instance
     */
    public ScreenshotPostProcessor(String _saveDirectory, WebDriver _driver) {
        mWebDriver       = _driver;
        mOutputDirectory = _saveDirectory;
    }


    /**
     * Process an array of broken nodes supplied typically by a Processor object.
     *
     * Traverses the Nodes parent structure and takes screenshots along the way to leave a trail of breadcrumbs for the
     * manual QA.
     *
     * @param _brokenNodes Arraylist of broken Node objects (Thrown errors)
     * @throws IOException if unable to take screenshot
     */
    public void postProcess(ArrayList<Node> _brokenNodes, ConsoleFileWatcher _watcher) throws IOException {
        String _outputDir;
        _watcher.writeLog("Taking screenshots...");
        for (Node broken : _brokenNodes) {
                Node _prev = null;
                if (new File(_outputDir = getNodeOutputDirectory(broken)).mkdirs()) {
                    for (Node current : broken.traceToRoot()) {
                        if (Arrays.asList(mIgnoreCodes).contains(current.getStatusCode())) {
                            _prev = current;
                            savePlaceholder(_outputDir, getGeneratedNodeName(current, broken.getDepth()), current.getStatusCode());
                        } else {
                            mWebDriver.navigate().to(current.getURL());
                            if (_prev != null) {
                                try {
                                    highlightElement(mWebDriver, _prev.getElementSelector());
                                } catch (Exception e){
                                    _watcher.writeLog("Failed to highlight element, could not find it on the page. Attempting screenshot..");
                                }
                            }
                            try {
                                saveScreenshot(mWebDriver, _outputDir, getGeneratedNodeName(current, broken.getDepth()));
                            } catch (Exception e){
                                _watcher.writeLog(String.format("Failed to take screenshot: %s. Continuing..", current.getURL()));
                            }
                            _prev = current;
                        }
                    }
                }
        }
        if(mWebDriver != null)
            mWebDriver.quit();

        _watcher.writeLog("Completed taking screenshots.");
    }

    /**
     * Return an output directory using the Nodes status code and url
     *
     * @param _node Node
     * @return Output directory string
     */
    private String getNodeOutputDirectory(Node _node){
       String _url  = FileNameCleaner.cleanFileName(_node.getURL());
       String _code = Integer.toString(_node.getStatusCode());

       if(_node.getStatusCode() == 0)
           _code = "0(ERROR)";

       return mOutputDirectory + "screenshots/" + _code + "/" + _url + "/";
    }
    /**
     * Highlight the WebElement found by the supplied CSS locator
     *
     * @param _driver WebDriver instance
     * @param _css CSS Locator
     */
    private void highlightElement(WebDriver _driver, String _css){
        JavascriptExecutor _js  = (JavascriptExecutor) _driver;
        WebElement _ele         = _driver.findElement(By.cssSelector(_css));

        if(_ele.isDisplayed()) {
            _js.executeScript("arguments[0].scrollIntoView(true);", _ele);
            _js.executeScript("arguments[0].style.border = \"2px solid red\"", _ele);
        } else {
            throw new ElementNotVisibleException("Failed to locate and highlight web-element. Element is not visible");
        }
    }

    /**
     * Return a generated name for the given node to be used in screenshotting
     * @param _node     Node object
     * @param _maxDepth Maximum depth
     * @return New name to save screenshot as
     */
    private String getGeneratedNodeName(Node _node, int _maxDepth){
        if(_node.getDepth() == _maxDepth){
            return "BrokenPage";
        } else {
            switch (_node.getDepth()) {
                case -1:
                    return "ExternalPage";
                case 0:
                    return "LandingPage";
                default:
                    return "Page_" + String.valueOf(_node.getDepth());
            }
        }
    }
    /**
     * Navigate to the given Node's URL and capture a Screenshot using the given WebDriver instance
     *
     * @param _driver   WebDriver instance
     * @param _saveDir  Output directory
     * @param _saveName Name to save picture as
     * @throws IOException if unable to save
     */
    private void saveScreenshot(WebDriver _driver, String _saveDir, String _saveName) throws IOException {
        String _path = _saveDir + _saveName + ".png";
        FileUtils.copyFile(((TakesScreenshot) _driver).getScreenshotAs(OutputType.FILE), new File(_path));
    }

    /**
     * Copy the placeholder image to the save directory.
     * The image saved is dependent on the support error codes (CURRENT ONLY 401)
     *
     * @param _saveDir  Output directory
     * @param _saveName Name to save picture as
     * @param _err      Error code
     * @throws IOException if unable to save
     */
    private void savePlaceholder(String _saveDir, String _saveName, int _err) throws IOException{
        String _imageName = null;
        switch(_err){
            case 401:
                _imageName = "401Placeholder.png";
                break;
        }

        if(_imageName == null) {
            return;
        }

        FileUtils.copyFile(new File("./resources/placeholder/" + _imageName), new File(_saveDir + _saveName + ".png"));
    }
}

