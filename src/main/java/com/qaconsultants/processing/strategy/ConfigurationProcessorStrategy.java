package com.qaconsultants.processing.strategy;

import com.qaconsultants.Config;

/**
 * Processor Strategy created using a Config object to supply
 * it with the appropriate information
 */
public class ConfigurationProcessorStrategy implements ProcessorStrategy{
    private int[]    mErrCodes;
    private String[] mIgnoredSelectors;
    private String[] mCSSSelectors;
    private String[] mURLSchemes;
    private String[] mIgnored;

    /**
     * Default Constructor
     * @param _config Config object
     */
    public ConfigurationProcessorStrategy(Config _config){
        mErrCodes     = _config.linkchecker.errorCodes;
        mCSSSelectors = _config.linkchecker.cssSelectors;
        mURLSchemes   = _config.linkchecker.validURLSchemes;
        mIgnored      = _config.linkchecker.ignoredContentTypes;
        mIgnoredSelectors = _config.linkchecker.urlPatternBlacklist;
    }

    @Override
    public String[] getCSSSelectors() {
        return mCSSSelectors;
    }

    @Override
    public String[] getURLBlacklist() {
        return mIgnoredSelectors;
    }

    @Override
    public String[] getIgnoredContentTypes() {
        return mIgnored;
    }

    @Override
    public String[] getURLSchemes() {
        return mURLSchemes;
    }

    @Override
    public int[] getFailureErrorCodes() {
        return mErrCodes;
    }
}
