package com.qaconsultants.processing.strategy;

/**
 * ProcessorStrategy is responsible for defining the characteristics of the Processor object.
 *
 * Concrete strategies must return the appropriate information for the Processor to build an appropriate
 * tree of valid links.
 */
public interface ProcessorStrategy {
    /**
     * Return a list of HTML tags to look-up during the Web-Element gathering process
     * @return Array of HTML tags, as Strings
     */
    String[] getCSSSelectors();

    /**
     * Return a list of CSS selectors used to ignore particular elements during processing.
     */
    String[] getURLBlacklist();

    /**
     * Return a list of content types to ignore during processing so as to not
     * download inappropriate file types. Ie; Application/octet-stream
     * @return Array of content-types, as Strings
     */
    String[] getIgnoredContentTypes();

    /**
     * Return an array of URL schemes to support
     * @return Array of URL schemes, as Strings
     */
    String[] getURLSchemes();

    /**
     * Return a list of HTML error codes to use as a reference for failure
     * @return Array of HTML error codes, as Strings
     */
    int[] getFailureErrorCodes();
}
