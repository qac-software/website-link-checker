package com.qaconsultants.processing;

import ch.sentric.URL;
import com.qaconsultants.processing.strategy.ProcessorStrategy;
import com.qaconsultants.processing.watcher.IWatcher;
import org.apache.commons.validator.routines.UrlValidator;
import org.jsoup.Connection;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.jsoup.select.Selector;
import org.openqa.selenium.WebDriver;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Internal class representing a DefaultProcessorStrategy.
 *
 * This is intentionally scoped to the Processor, as we do not want the end-user to have access to this component.
 * It is encouraged for them to define their own Processor strategy.
 */
class DefaultProcessorStrategy implements ProcessorStrategy {
    @Override
    public String[] getCSSSelectors() {
        //a     - Anchor Tags
        //input - Buttons/Input Methods
        return new String[]{"a[href]"};
    }

    @Override
    public String[] getURLBlacklist() {
        return new String[0];
    }

    @Override
    public String[] getIgnoredContentTypes() {
        // application/octet-stream - Downloadable content we do not want
        return new String[]{"application/octet-stream"};
    }

    @Override
    public String[] getURLSchemes() {
        //HTTP - Unsecure
        //HTTPS- Secure
        //File - Local file
        //FTP  - File Transfer Protocol
        return new String[]{"http", "https", "file", "ftp"};
    }

    @Override
    public int[] getFailureErrorCodes() {
        //0   - Tool Specific Unhandled Error Code
        //400 - Bad Request
        //401 - Unauthorized
        //404 - Not Found
        //408 - Server Timeout
        //418 - I'm a teapot
        //500 - Internal Server Error
        return new int[]{0, 400, 401, 404, 408, 418, 500};
    }
}

/**
 *
 * @author tremazki
 * Processes a Node and its children based on a specified depth.
 *
 * Processing a node involves verifying its response header for appropriate return codes and
 * filtering out the 'failure' error codes as described in the Processor strategy.
 *
 * Additional nodes are found and appended as children to the initial node, and the process
 * is run until a maximum depth has been accomplished or all children have had all their child nodes verified.
 *
 */
public class Processor {

    /**
     * A ResultSet is the aggregate results of the Processors run
     */
    public static class ResultSet {
        ResultSet(ArrayList<Node> _all, ArrayList<Node> _brk, ArrayList<Node> _ext, ArrayList<Node> _ign){
            ignoredNodes       = _ign;
            foundNodes         = _all;
            foundBrokenNodes   = _brk;
            foundExternalNodes = _ext;
        }

        public ArrayList<Node> foundNodes;
        public ArrayList<Node> foundBrokenNodes;
        public ArrayList<Node> foundExternalNodes;
        public ArrayList<Node> ignoredNodes;
    }

    /**
     * Internal static class used for configuring the Processor
     */
    public static class Configuration {
        public int     maxDepth      = 1;
        public int     sleepTimeInMS = 500;

        public void setDepth(int _depth) {
            if (_depth >= 0) {
                maxDepth = _depth;
            } else {
                maxDepth = 1;
                System.out.println("\n[WARN::CONFIG] Invalid maximum depth - defaulting to 1");
            }
        }

        public void setSleep(int _ms) {
            if (_ms >= 500) {
                sleepTimeInMS = _ms;
            } else {
                sleepTimeInMS = 1000;
                System.out.println("\n[WARN::CONFIG] Invalid sleep time - defaulting to 1000");
            }
        }
    }

    private CopyOnWriteArrayList<Node> mNodeList;
    private ArrayList<String>          mURLList;
    private ArrayList<Node>            mBrokenNodes;
    private ArrayList<Node>            mExternalNodes;
    private ArrayList<Node>            mIgnoredNodes;
    private UrlValidator               mUrlValidator;

    protected ProcessorStrategy strategy;
    protected Configuration     config;

    /**
     * Default Constructor
     *
     * @param _strategy Strategy defining the capabilities of this processor.
     */
    public Processor(ProcessorStrategy _strategy, Configuration _config) {
        config         = _config;
        strategy       = _strategy;
        mBrokenNodes   = new ArrayList<>();
        mExternalNodes = new ArrayList<>();
        mURLList       = new ArrayList<>();
        mIgnoredNodes  = new ArrayList<>();
        mNodeList      = new CopyOnWriteArrayList<>();
        mUrlValidator  = new UrlValidator(strategy.getURLSchemes(),
                                    UrlValidator.ALLOW_LOCAL_URLS +
                                           UrlValidator.ALLOW_2_SLASHES);

    }

    /**
     * Constructor
     */
    public Processor(Configuration _config) {
        this(new DefaultProcessorStrategy(), _config);
    }

    /**
     * Processes the given root node and subsequent child nodes
     * that are appended during the process period.
     *
     * @param _root Node instance representing the ROOT element
     * @param _watcher The IWatcher to use
     */
    public ResultSet process(Node _root, WebDriver _driver, IWatcher _watcher) throws Exception {
        /* Append the root node to both the node list and the URL list */
        mNodeList.add(_root);
        mURLList.add(new URL(_root.getURL()).getNormalizedUrl());

        try {
            Node    _current;
            boolean _collect;

            // Signal our watcher
            _watcher.beginProcessor(config, strategy, _root);

            /* Loop over the list of nodes to check */
            for(int i = 0; i < mNodeList.size(); i++) {

                /* Indicate the watcher of a node beginning and ending processing */
                _collect = false;
                _current = mNodeList.get(i);
                if(verifyNode(_current)){
                    _watcher.beginProcessingNode(_current);

                    processNode(_current);

                    _watcher.endProcessingNode(_collect = checkNodeValidity(_current), _current.getStatusCode());
                }

                if(_current.exception != null)
                    _watcher.handleNodeException(_current.exception);

                /*
                 * Ensure the depth limit is within the constraints.
                 * If the node is considered valid, collect child urls and append to the node list
                 */
                if (_collect && (_current.getDepth() < config.maxDepth || config.maxDepth == 0)) {
                        mNodeList.addAll(collectChildren(_driver, _current, _watcher));
                }

                /*
                 * Sleep for an externally configured amount of time.
                 * Single threaded application, so this should be safe.
                 */
                Thread.sleep(config.sleepTimeInMS);
            }
        } catch (Exception e) {
            _watcher.handleProcessorException(e);
            throw e;
        } finally {
            if(_driver != null) {
                _driver.quit();
            }
        }
        return new ResultSet(new ArrayList<>(mNodeList), mBrokenNodes, mExternalNodes, mIgnoredNodes);
    }

    /**
     * Reconstruct the URL into the base URL
     *
     * @param _url Original URL
     * @return Base URL
     */
    private String extractBaseURL(String _url){
        try {
            URI _uri = new URI(_url);
            return _uri.getHost().startsWith("www.") ? _uri.getHost().substring(4) : _uri.getHost();
        } catch (Exception e){
            return _url;
        }
    }

    /**
     * Verify the validity of the given Node
     *
     * @param _node Node
     */
    private boolean verifyNode(Node _node){
        if(_node == null || _node.getURL() == null)
            return false;
        return mUrlValidator.isValid(_node.getURL());
    }

    /**
     * Process the given Node using JSoup.
     *
     * Verifies the return code, content type, and depth against the supplied externally
     * configured information
     *
     * @param _node Node to process
     */
    private void processNode(Node _node) {
        if (_node.getChildren().isEmpty() && !_node.getProcessed()) {
            Connection.Response _response;
            Connection          _connection = Jsoup.connect(_node.getURL()).ignoreContentType(true);

            int _status = 0;
            try {
                _response = _connection.execute();
                _status   = _response.statusCode();
                _node.setContentType(_response.contentType());
            } catch (HttpStatusException e) {
                _status = e.getStatusCode();
            } catch (SocketTimeoutException | ConnectException e) {
                _status = 408;
            } catch (Exception e) {
                e.printStackTrace();
                _node.exception = e;
            }
            _node.setProcessed(true);
            _node.setStatusCode(_status);
        }
    }

    /**
     * Check the validity of the Node against Rules defined prior to running the Processor
     *
     * TODO MOVE OUTSIDE AND CLEAN
     *
     * @param _node Node to check for validity
     * @return True if valid, False otherwise. True implies that we may gather additional links from this Node.
     */
    private boolean checkNodeValidity(Node _node) {
            /* Url Blacklist RULE. If ends with a wildcard, exclude all links containing it */
            // --------------------------------------------------------------------------
            boolean _ignore = false;
            if(strategy.getURLBlacklist().length > 0) {
                for (String _sel : strategy.getURLBlacklist()) {
                    if (_sel.endsWith("*")) {
                        String _sub = _sel.substring(0, _sel.length() - 1);
                        if (_node.getURL().startsWith(_sub)) {
                            _ignore = true;
                            break;
                        }
                    } else {
                        if (_node.getURL().equals(_sel)) {
                            _ignore = true;
                            break;
                        }
                    }
                }
            }

            if(_ignore) {
                mIgnoredNodes.add(_node);
                if(_node.getDepth() == -1)
                    mExternalNodes.add(_node);
                return false;
            }
            // --------------------------------------------------------------------------

            /*
             * Validate the response status against the defined Processor strategy.
             * If an expected error code is found, add to the list of broken nodes and return.
             */
            if(strategy.getFailureErrorCodes().length > 0)
                for (int err : strategy.getFailureErrorCodes()) {
                    if (_node.getStatusCode() == err) {
                        if(_node.getDepth() == -1)
                            mExternalNodes.add(_node);
                        mBrokenNodes.add(_node);
                        return false;
                    }
                }

            /*
             * Verify the response header to ensure that we don't set this node for further processing
             * if its content type is part of the ignored set in the configuration file
             */
            if(strategy.getIgnoredContentTypes().length > 0)
                for(String ignore : strategy.getIgnoredContentTypes()){
                    if(_node.getContentType() != null)
                        if(_node.getContentType().equals(ignore))
                            return false;
                }

            /* Return false if the URL contains a fragment, as we don't want to attempt to scrape this. */
            if(_node.getURL().contains("#"))
                return false;

            if(_node.getDepth() == -1) {
                mExternalNodes.add(_node);
                return false;
            }

        return true;
    }

    /**
     * This function discovers additional child nodes and appends them to the
     * supplied Node object.
     *
     * @param _node Node
     */
    private ArrayList<Node> collectChildren(WebDriver _driver, Node _node, IWatcher _watcher) throws Exception {
        /* Create a temporary array list for storing new children */
        ArrayList<Node> _children = new ArrayList<>();

        /*
         * Collect the document using a Jsoup Connection object
         */
        _driver.get(_node.getURL());
        Document _document = Jsoup.parse(_driver.getPageSource(), _node.getURL());
        /*
         * Loop over the CSS selectors defined by the Strategy and append
         * to a list of Elements
         */
        Elements _links = new Elements();
        for (String tag : strategy.getCSSSelectors())
            _links.addAll(_document.select(tag));

        /*
         * Loop over the elements of the page, found by the specified CSS selectors
         * and extract their href attribute.
         *
         * Use this href value as a key to store as a Node object.
         *
         * Depth is used to indicate the hierarchy of many-to-many node relationships.
         * A depth of -1 indicates an external link.
         */
        URI    _hrefURL;
        int    _depth;
        Node   _child;
        String _href;
        String _key;
        String _locator;
        String _url = extractBaseURL(_node.getURL());
        for (Element ele : _links) {
            _depth = -1;
            _href  = ele.attr("abs:href");

            // Ignore HREF values that are blank or have a 'mailto' url schema
            if(_href.equals("") || _href.startsWith("mailto:")) {
                continue;
            }

            /*
             * If this website is considered internal, we append the proper depth.
             * A depth of -1 indicates an end-point for the processor and prevents the gathering
             * of addition nodes/children
             *
             */
            if (_url.equals(extractBaseURL(_href)))
                _depth = _node.getDepth() + 1;

            /*
             * If the node map does not contain this key,
             * we create a new unique child and append it to the current node.
             *
             * All nodes contain UNIQUE children. No duplicates.
             * This is intentional, as we only need a single path to a broken link.
             */
            try {
                _hrefURL = new URI(_href);
                _key     = new URL(_hrefURL).getNormalizedUrl();
                _href    = _hrefURL.normalize().toURL().toString();
            } catch (Exception e) {
                _key     = new URL(_href).getNormalizedUrl();
            }

            if (!mURLList.contains(_key)) {
                mURLList.add(_key);
                try {
                    _locator = ele.cssSelector();
                } catch (Selector.SelectorParseException e){
                    _locator = null;
                    _watcher.writeError("\nAn error occurred getting the CSS locator from an element. This will only affect screenshots. Link will still be processed.");
                    _watcher.writeError(String.format("Element HREF %s"));
                }
                _child = new Node(_node, _href, _depth, _locator);
                _children.add(_child);
                _node.addChild(_child);
            }
        }
        return _children;
    }
}
