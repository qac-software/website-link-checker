package com.qaconsultants.processing.watcher;

import com.qaconsultants.processing.Node;
import com.qaconsultants.processing.Processor;
import com.qaconsultants.processing.strategy.ProcessorStrategy;

import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * IWatcher is used to monitor the Processor
 */
public interface IWatcher {
    /**
     * Called at the beginning of the processor
     */
    void beginProcessor(Processor.Configuration _config, ProcessorStrategy _strat, Node _root);

    /**
     * Called at the end of the processor
     */
    void endProcessor(Processor.ResultSet _set);

    /**
     * Called when beginning to process a Node object
     * @param _n
     */
    void beginProcessingNode(Node _n);

    /**
     * Called when ending the processing of a Node object
     */
    void endProcessingNode(boolean _pass, int _status);

    /**
     * Handle any node related exception during the processing.
     * @param _t
     */
    void handleNodeException(Throwable _t);

    /**
     * Handle any exception during the processing
     * @param _t
     */
    void handleProcessorException(Throwable _t);

    void writeLog(String _msg);

    void writeError(String _msg);
}
