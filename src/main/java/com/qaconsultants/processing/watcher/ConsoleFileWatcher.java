package com.qaconsultants.processing.watcher;

import com.qaconsultants.App;
import com.qaconsultants.processing.Node;
import com.qaconsultants.processing.Processor;
import com.qaconsultants.processing.strategy.ProcessorStrategy;
import org.jsoup.HttpStatusException;
import org.openqa.selenium.io.MultiOutputStream;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.concurrent.CopyOnWriteArrayList;

public class ConsoleFileWatcher implements IWatcher {

    private FileOutputStream  mURLStream;

    private PrintStream       mConsoleStream,
                              mErrStream;

    private Instant           mStart,
                              mEnd;

    // TODO CLEANUP
    // TODO Separate IWatcher into IWatcher and IResultParser for results only. Watcher should do run-time reporting.
    public ConsoleFileWatcher(String _saveLocation) throws FileNotFoundException{

        try{
            Path _logLocation = Paths.get(_saveLocation);
            Files.createDirectories(_logLocation);
        } catch (Exception e) {
            e.printStackTrace();
        }
        mConsoleStream = new PrintStream(new MultiOutputStream(new FileOutputStream(_saveLocation + "Ledger.txt"), System.out));
        mErrStream     = new PrintStream(new MultiOutputStream(new FileOutputStream(_saveLocation + "Errors.txt"), mConsoleStream));
        mURLStream     = new FileOutputStream(_saveLocation + "Summary.txt");
    }

    @Override
    public void beginProcessor(Processor.Configuration _config, ProcessorStrategy _strat, Node _root) {
        StringBuilder _sb = new StringBuilder();
        _sb.append(printHeader(_config, _root));
        try {
            mConsoleStream.println(_sb.toString());
            mURLStream.write(_sb.toString().getBytes());
        } catch (Exception e){
            e.printStackTrace();
        }
        mConsoleStream.print("Beginning Processor..\n");
    }

    @Override
    public void endProcessor(Processor.ResultSet _set) {
        StringBuilder _builder = new StringBuilder();
        _builder.append("\n---------------------------------------");
        _builder.append(String.format("\nTotal: %d", _set.foundNodes.size()));
        _builder.append(String.format("\nInternal Link Total: %d", _set.foundNodes.size() - _set.foundExternalNodes.size()));
        _builder.append(String.format("\nExternal Link Total: %d", _set.foundExternalNodes.size()));
        _builder.append("\n---------------------------------------");

        mConsoleStream.println("\nExiting Processor");
        mConsoleStream.println(_builder.toString());
        mConsoleStream.println(printSummary(_set.foundBrokenNodes));
        mConsoleStream.println(printInstructions());
        try {
            mURLStream.write(printNodeMap(_set.ignoredNodes,
                                          _set.foundExternalNodes,
                                          _set.foundBrokenNodes,
                                          _set.foundNodes).getBytes());
            mURLStream.write(printSummary(_set.foundBrokenNodes).getBytes());
            mURLStream.close();
        } catch (Exception e){
            mErrStream.println("Failed to write URL list");
        }
    }

    @Override
    public void beginProcessingNode(Node _n) {
        mStart = Instant.now();
        mConsoleStream.println(String.format("\nFetching URL : %s", _n.getURL()));
    }

    @Override
    public void endProcessingNode(boolean _pass, int _status) {
        mEnd = Instant.now();
        mConsoleStream.println(String.format("Response Code: %d", _status));
        mConsoleStream.println(String.format("Response Time: %s", Duration.between(mStart, mEnd).toString().substring(2)));
    }

    @Override
    public void handleNodeException(Throwable _t) {
        if(_t == null)
            return;

        if(_t instanceof HttpStatusException)
            if(((HttpStatusException) _t).getStatusCode() == 401)
                mErrStream.println("URL requires authentication.");

        mErrStream.println("An error has occurred while processing the Node. See the stacktrace for more information.");
        _t.printStackTrace(mErrStream);
    }

    @Override
    public void handleProcessorException(Throwable _t) {
        if(_t == null)
            return;

        _t.printStackTrace(mErrStream);
    }

    public static String printHeader(Processor.Configuration _config, Node _root){
        StringBuilder _sb = new StringBuilder();
        _sb.append(String.format("Link Checker Tool - Version: %s\n", App.version));
        _sb.append("=======================================\n");
        _sb.append(String.format("URL: \t%s\n", _root.getURL()));
        _sb.append(String.format("Depth: \t%d\n", _config.maxDepth));
        _sb.append(String.format("Date: \t%s\n", new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime())));
        _sb.append(String.format("Time: \t%s\n", new SimpleDateFormat("HH-mm-ss").format(Calendar.getInstance().getTime())));
        _sb.append("\n=======================================\n");
        return _sb.toString();
    }
    /**
     * Print a basic summary of the Processor's nodeMap
     * For our usage, we print out the URL of all Nodes accessed and stored by the Processor
     *
     */
    public static String printNodeMap(ArrayList<Node> _ignored, ArrayList<Node> _external, ArrayList<Node> _broken, ArrayList<Node> _nodeList){
        if(_nodeList.size() == 0)
            return "";

        CopyOnWriteArrayList<Node> _internal = new CopyOnWriteArrayList<>(_nodeList);
        _internal.removeAll(_external);

        ArrayList<Node> _brokenInternal = new ArrayList<>(_broken);
        _brokenInternal.retainAll(_internal);

        ArrayList<Node> _brokenExternal = new ArrayList<>(_broken);
        _brokenExternal.retainAll(_external);

        StringBuilder _builder = new StringBuilder();
        _builder.append("=======================================");
        _builder.append("\nBlacklisted/Ignored URLs:");
        _builder.append("\nTotal: " + _ignored.size());
        _builder.append("\n\nDisclaimer: Links were not scraped from these pages, as they matched a URL specified in your URL blacklist." +
                " Please check your configuration file if this was not intentional.");
        _builder.append("\n\nIgnored URLs:");
        _builder.append("\n----------------------------------\n");
        if(_ignored.size() > 0) {
            for (Node n : _ignored) {
                _builder.append(n.getURL());
                _builder.append("\n");
            }
        } else {
            _builder.append("NO IGNORED LINKS FOUND");
            _builder.append("\n");
        }
        _builder.append("\n=======================================");
        _builder.append("\n=======================================");
        _builder.append("\nDetected Internal URLs:");
        _builder.append("\nTotal: " + _internal.size());
        _builder.append("\n\nAll URLs including broken/ignored:");
        _builder.append("\n----------------------------------\n");
        for(Node n : _internal){
            _builder.append(n.getURL());
            _builder.append("\n");
        }
        _builder.append("\nBroken URLs:");
        _builder.append("\n----------------------------------\n");
        if(_brokenInternal.size() > 0) {
            for (Node n : _brokenInternal) {
                _builder.append(n.getURL());
                _builder.append("\n");
            }
        } else {
            _builder.append("NO BROKEN LINKS FOUND");
            _builder.append("\n");
        }
        _builder.append("\n=======================================");
        _builder.append("\n=======================================");
        _builder.append("\nDetected External URLs:");
        _builder.append("\nTotal: " + _external.size());
        _builder.append("\n\nDisclaimer: Number of external links may fluctuate between runs regardless of if the same depth is used." +
                                      " This is currently being looked into by the developer(s).");
        _builder.append("\n\nAll URLs including broken/ignored:");
        _builder.append("\n----------------------------------\n");
        for(Node n :_external){
            _builder.append(n.getURL());
            _builder.append("\n");
        }
        _builder.append("\nBroken URLs:");
        _builder.append("\n----------------------------------\n");
        if(_brokenExternal.size() > 0) {
            for (Node n : _brokenExternal) {
                _builder.append(n.getURL());
                _builder.append("\n");
            }
        } else {
            _builder.append("NO BROKEN LINKS FOUND");
            _builder.append("\n");
        }
        _builder.append("\n=======================================");
        _builder.append("\n=======================================");
        return _builder.toString();
    }

    /**
     * Print a basic summary of the Processor's broken nodes
     *
     */
    public static String printSummary(ArrayList<Node> brokenNodes){
        brokenNodes.sort(new Comparator<Node>() {
            @Override
            public int compare(Node o1, Node o2) {
               return o1.getStatusCode() - o2.getStatusCode();
            }
        });

        StringBuilder _builder = new StringBuilder();
        if(brokenNodes.size() > 0) {
            _builder.append("\nAll Broken URLs (Internal + External):");
            _builder.append("\nTotal: " + brokenNodes.size());
            _builder.append("\n\nBroken URLs:");
            _builder.append("\n----------------------------------");
            if(brokenNodes.size() > 0) {
                for (Node n : brokenNodes) {
                    _builder.append(String.format("\n[%s]: %s", n.getStatusCode(), n.getURL()));
                    _builder.append(String.format("\n\t[%s]: %s", "Source", n.getParent().getURL()));
                    _builder.append(String.format("\n\t\t[Broken Element Selector]: %s", n.getElementSelector()));
                    _builder.append("\n");
                }
            } else {
                _builder.append("NO BROKEN LINKS FOUND");
                _builder.append("\n");
            }
            _builder.append("\n=======================================");
            _builder.append("\n=======================================");
        }
        return _builder.toString();
    }

    public static String printInstructions(){
        StringBuilder _builder = new StringBuilder();
        _builder.append("\nPlease verify the broken URLs above manually by doing the following:");
        _builder.append("\n\n\t1) Manually copy/paste the broken link into the URL field of your browser and navigate." +
                "\n\tIf the link is indeed broken, continue to the next set of instructions. Otherwise, please open a bug with the developer.");

        _builder.append("\n\nIf the screenshots were taken properly...");
        _builder.append("\n\n\t1) Manually follow the highlighted elements in the screenshots to navigate back to the broken link");
        _builder.append("\n");

        _builder.append("\nIf the screenshots were NOT taken or failed to be taken...");
        _builder.append("\n\n\t1) Manually copy/paste the link denoted by [Source] under the broken link and navigate to the link in Chrome/Firefox of choice. " +
                "\n\t2) Manually copy the CSS selector denoted by [Broken Element Selector] under the [Source] page." +
                "\n\t3) Press F12 in Chrome/Firefox to open the developer console" +
                "\n\t4) Press CTRL+F, paste the CSS selector into the search box, and press the Enter key." +
                "\n\t5) The element should be highlighted in the DOM in the console, as well as on the web-page for you to see.");
        _builder.append("\n");
        return _builder.toString();
    }

    public void writeError(String _err){
        mErrStream.println(_err);
    }

    public void writeLog(String _msg){
        mConsoleStream.println(_msg);
    }
}
