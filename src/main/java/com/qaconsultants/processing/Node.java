package com.qaconsultants.processing;

import java.util.ArrayList;

enum TYPE {
    INTERNAL,
    EXTERNAL,
    IGNORED
}
/**
 * A node is a data representation of a potential path any given hyperlink/misdirection
 * may push the user on during functional testing.
 * <p>
 * It maintains a list of parents and list of children.
 * <p>
 * A unique identifier is given to each and every node, and is generated by _INSERT SELECTION METHOD HERE_
 *
 * A NULL parent + 0 depth signifies a ROOT node.
 */
public class Node {
    private int             mStatusCode      = 0;
    private int             mDepth           = 0;
    private boolean         mProcessed       = false;
    private Node            mParent          = null;
    private String          mURL             = null;
    private String          mCSSSelector     = null;
    private String          mContentType     = null;
    private ArrayList<Node> mChildren        = null;

    public Exception exception = null;

    /**
     * Constructor
     *
     * @param _parent Parent Node - Null if ROOT
     */
    public Node(Node _parent, String _url, int _depth){
        mChildren        = new ArrayList<>();
        mURL             = _url;
        mDepth           = _depth;
        mParent          = _parent;
    }

    public Node(Node _parent, String _url, int _depth, String _selector){
        this(_parent, _url, _depth);
        mCSSSelector = _selector;
    }

    /**
     * Troll through the parent Node objects until we no longer find one, or it is null.
     *
     * @return List of Node objects making up a trace to the root Node
     */
    public ArrayList<Node> traceToRoot(){
        ArrayList<Node> _trace  = new ArrayList<>();
        Node            _parent = getParent();

        _trace.add(this);

        while(_parent != null) {
            _trace.add(_parent);
            _parent = _parent.getParent();
        }
        return _trace;
    }

    /**
     * Add a child Node to this Node instance
     *
     * @param _node Node instance
     */
    public void addChild(Node _node){
        mChildren.add(_node);
    }

    /**
     * returns the children of this Node instance.
     * 
     * @return the Children of this Node instance.
     */
    public ArrayList<Node> getChildren(){
        return mChildren;
    }

    /**
     * Returns the parents associated with this Node instance.
     * Parent nodes are immutable and guarantee a valid link.
     *
     * @return Parent node
     */
    public Node getParent() {
        return mParent;
    }

    /**
     * Returns the URL associated with this Node instance.
     *
     * @return URL
     */
    public String getURL(){
        return mURL;
    }

    public void setURL(String _url){
        mURL = _url;
    }

    /**
     * Returns the depth of this Node in the Node tree
     *
     * @return Depth as an integer
     */
    public int getDepth(){
        return mDepth;
    }

    /**
     * Get the selector used to locate an element leading to this node.
     *
     * @return Selector string
     */
    public String getElementSelector(){
        return mCSSSelector;
    }

    /**
     * Sets the content type of this Node to the one supplied
     *
     * @param _type Content Type as a String
     */
    public void setContentType(String _type) { mContentType = _type;}

    /**
     * Return the Content Type associated with this Node.
     *
     * @return Content Type as a String
     */
    public String getContentType() { return mContentType;}

    /**
     * Sets the status code of this Node to the one supplied
     *
     * @param status Status code
     */
    public void setStatusCode(int status){
        mStatusCode = status;
    }
    /**
     * Return the status code associated with this Node instance, if any
     *
     * @return Status code
     */
    public int getStatusCode(){
        return mStatusCode;
    }

    /**
     * Set the visited status of this Node object
     *
     * @param _bool
     */
    public void setProcessed(boolean _bool){
        mProcessed = _bool;
    }

    /**
     * Return whether or not this Node has been visited already
     *
     * @return Boolean
     */
    public boolean getProcessed() {
        return mProcessed;
    }
}
