# NOTES

## Invisible / Inactive Anchors
---
Occassionally, web-developers will hide unused links in inconsistent ways. We must either find a way to address these links - Whether by reporting them separately or giving the end-user more control through blacklists.

- Jesses Solutions
  - Do not search/validate links with an empty href
  - Do not search/validate links with empty inner text (Typically a hidden element)

## Discriminate URL Patterns
---
Occassionally, elements on the given page may lead to undesired web-pages. A common example of this is a top-navigation element used to change languages to other locales. We might not want to continue drilling down and collecting children of these websites.

- Possible Solutions
  - URL pattern blacklist
    - Ie; Do not gather children from links starting with.. www.myWebsite.com/ru (russian), as I only want links from www.myWebsite.com/en
