FROM maven:alpine

# create app dir
WORKDIR /usr/src/app

COPY bin/ ./bin

RUN mvn install:install-file -Dfile=/usr/src/app/bin/url-normalization-1.0.0-jar-with-dependencies.jar -DgroupId=ch.sentric -DartifactId=url-normalization -Dversion=1.0.0 -Dpackaging=jar -DgeneratePom=true

COPY resources ./resources
# have to copy these files over all at once
COPY Instructions.txt ChangeLog.txt Run.bat assembly.xml pom.xml ./

RUN mvn dependency:resolve-plugins dependency:resolve clean install

COPY . ./

RUN mvn package assembly:single -Dmaven.test.skip=true -e

ENTRYPOINT [ "java", "-jar", "build/link-checker-1.1.1-jar-with-dependencies.jar"]

